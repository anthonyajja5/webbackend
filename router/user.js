const express = require('express')
const router = express.Router()
const mysql = require('mysql')
const bcrypt = require('bcrypt')
const saltRounds = 10
const auth = require('../middlewares/auth.js')
router.use(express.json())
router.use(express.urlencoded())

var conn = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'anthony'
})

conn.connect(function(err){
    if(err)
        throw err
})

router.post('/',function(req,res,next){
    const sql = `SELECT * FROM items`
    conn.query(sql, function(err,result){
        if(err)
            throw err
        if(result.length > 0)
            auth(req,res,next)
        else
            next()
    })
},function(req,res){
    bcrypt.genSalt(saltRounds, function (err, salt) {
        bcrypt.hash(req.body.password, salt, function (err, hash) {
            const sql = "INSERT INTO items(nama, password) VALUES ('" + req.body.username + "' , '" + hash + "')"
            conn.query(sql, function (err) {
                if (err) throw err
                console.log("Insert Success")
                res.end()
            })
        })
    })
})

// router.post('/', function (req,res){
//     bcrypt.genSalt(saltRounds, function (err, salt) {
//         bcrypt.hash(req.body.password, salt, function (err, hash) {
//             const sql = "INSERT INTO items(nama, password) VALUES ('" + req.body.username + "' , '" + hash + "')"
//             conn.query(sql, function (err) {
//                 if (err) throw err
//                 console.log("Insert Success")
//                 res.end()
//             })
//         })
//     })
// })

router.get('/',function(req,res){
    const sql = 'SELECT * FROM items'
    conn.query(sql,function(err,result){
        if(err)
            throw err
        res.send(result)
    })
})

router.delete('/:id',(req,res,next) => {
    const sql = 'SELECT * FROM items'
    conn.query(sql, function(err,result){
        if(err)
            throw err
        else if(result.length > 1)
            next()
        else
            res.sendStatus(401)
    })
},function(req,res){
    const sql = `DELETE FROM items WHERE id=\'${req.params.id}\'`
    conn.query(sql,function(err,result){
        if(err)
            throw err
        res.send("Delete Success")
    })
})

module.exports = router